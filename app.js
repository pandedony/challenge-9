const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');


const app = express();

var corsOptions = {
  origin: "http://localhost:8000"
};

app.use(cors(corsOptions));


app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.post("/post", (req, res) => {
  console.log("Connected to React");
  res.redirect("/");
});


//Server Running 
const PORT = 8000;
app.listen(PORT, () => {
    console.log(`Server Running on http://localhost:${PORT}`)
})